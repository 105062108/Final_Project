var EndState = {
    preload: function(){
        game.load.spritesheet("dragon",'assets/img/dragon.png', 258, 209,25);
        game.load.spritesheet("archer",'assets/img/archer.png', 158, 173,48);
    },
    create: function(){
        this.player = game.add.sprite(-200, 200,'archer'); 
        this.player.animations.add('walk', [ 20,19,18,17,16,31,30,29,28,27,26,25,24,39,38,37,36,35,34,33,32,47,46,46,46,46], 8, false);

        this.dragon = game.add.sprite(-50, 200,'dragon'); 
        this.dragon.animations.add('walk', [ 5, 6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21], 8, false);
        this.text = game.add.text(32, 32, '', { font: "15px Arial", fill: "#19de65" });
    },
    update: function(){
        
        var style = {font: "40px Papyrus", fill: "#ff0044" }

        this.dragon.animations.play('walk',5);
        this.dragon.x+=2;
        if(this.dragon.x>300){
            if(this.player.x<200)
                this.player.x += 5;
            else{
                this.player.animations.play('walk',10, false, true);
                title = game.add.text(200, 250, "", style);
                setTimeout(function(){
                    title.setText("The End ");
    
                },3000);
                setTimeout(function(){
                    game.state.start('menu');
    
                },4000);
            }
        }
    }
};
