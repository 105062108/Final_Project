var GameMenu = function() {};


GameMenu.prototype = {

  menuConfig: {
    startY: 260,
    startX: 30
  },

  init: function () {
    this.titleText = game.make.text(game.world.centerX+37, 100, "Block Hero", style.title.default);
    this.titleText.setShadow(3, 3, 'rgba(0.5,0.5,0.5,0.5)', 5);
    this.titleText.anchor.set(0.5,0.5);
    this.optionCount = 1;
  },
  preload: function () {
    game.load.image('background', 'assets/img/block.jpg');
    //game.load.image('test', 'assets/img/yellow_block.png');

    game.load.image('lock', 'assets/img/lock.png');
    game.load.image('block1', 'assets/item/blue block.png');
    game.load.image('block2', 'assets/item/green block.png');
    game.load.image('block3', 'assets/item/lightblue block.png');
    game.load.image('block4', 'assets/item/orange block.png');
    game.load.image('block5', 'assets/item/purple block.png');
    game.load.image('block6', 'assets/item/red block.png');
    game.load.image('block7', 'assets/item/yellow block.png');
    game.load.image('floor', 'assets/item/ghost block.png');
    game.load.image('blocks1', 'assets/item/blue_block.png');
    game.load.image('blocks2', 'assets/item/green_block.png');
    game.load.image('blocks3', 'assets/item/lightblue_block.png');
    game.load.image('blocks4', 'assets/item/orange_block.png');
    game.load.image('blocks5', 'assets/item/purple_block.png');
    game.load.image('blocks6', 'assets/item/red_block.png');
    game.load.image('blocks7', 'assets/item/yellow_block.png');
    //game.load.spritesheet('snake','assets/monster/slime-Sheet.png',32,25);
    game.load.spritesheet('mushroom', 'assets/monster/mushroom.png', 192, 153);
    game.load.spritesheet('snake', 'assets/monster/snake-147x94.png', 147, 94);
    game.load.spritesheet('player', 'assets/char/warrior.png', 182, 123);

    game.load.image('bg', 'assets/item/bg.png');
    game.load.image('rpg_bg', 'assets/background/11.png');
    game.load.image('line', 'assets/background/shape.png');
    game.load.image('row', 'assets/background/shape2.png');

    game.load.image('life_bg', 'assets/item/lifeShell.png');
    game.load.image('life_bar', 'assets/item/lifebar.png');

    game.load.image('hp1', 'assets/item/player_hpbar1.png');
    game.load.image('hp2', 'assets/item/player_hpbar2.png');
    game.load.image('hpshell', 'assets/item/player_hpShell.png');

    game.load.spritesheet('atk1', 'assets/fx/basic_attack.png', 117, 102);
    game.load.spritesheet('atk2', 'assets/fx/attack2.png', 117, 102);
    game.load.spritesheet('atk4', 'assets/fx/attack4.png', 102, 96);
    //UI
    game.load.image('char', 'assets/UI/char.png');
    game.load.image('basic_atk', 'assets/UI/basic_atk.png');
    game.load.image('skill2', 'assets/UI/skl2.png');
    game.load.image('ultimate', 'assets/UI/ult.png');
    game.load.image('shield', 'assets/UI/shield.png');
    game.load.spritesheet('num', 'assets/images/num.png', 20, 20);
    game.load.image('black','assets/images/black.png');
    game.load.audio('bgm', 'assets/bgm/theme-7.ogg');
    game.load.audio('two', 'assets/bgm/1.ogg');
    game.load.audio('one', 'assets/bgm/2.ogg');
  },
  create: function () {
    
    this.cursor = game.input.keyboard.createCursorKeys();

    game.add.image(0, 21, 'background_in');
    this.player = game.add.sprite(290,380,'warrior');
    this.player.animations.add('idle',[0,1,2,3,4,5],8,true);
    this.player.animations.add('damage',[6,7,8,9,10,11],8,false);
    this.player.animations.play('idle'); 

    this.music =game.add.audio('dangerous');
    music.play();

    if (music.name !== "dangerous" && gameOptions.playMusic) {
      music.stop();
      music = game.add.audio('dangerous');
      music.loop = true;
      music.play();
    }
    game.stage.disableVisibilityChange = true;
    // game.add.sprite(0, 0, 'menu-bg');
    game.stage.backgroundColor = cs.background_color
    game.add.existing(this.titleText);
  

    // create a Menu group - only use full if we want to auto adjust the entire menu
    this.menuGroup = game.add.group();

    this.addMenuOption('\uf11b Advance ', function () {
      music.stop();
      game.state.start("map");
    }, 'default' , "fa_style", this.menuGroup);

    this.addMenuOption('Options \uf013', function () {
      game.state.start("option");
    }, 'default' , "fa_style", this.menuGroup);

    this.addMenuOption('\uf091 About', function () {
      game.state.start("Credits");
    }, 'default' , "fa_style", this.menuGroup);
    
    this.adjustBottom(20, 0, this.menuGroup);
    console.log(this.menuGroup);

    /* adding UI icon*/
    this.RotatingIcon = this.add.text( 52, 100, '\uf23c', { fill : cs.accent_color, font : '64px FontAwesome' });
    this.RotatingIcon.anchor.setTo(0.5);	

    //this.randomRotatingIcon = this.add.text( 350, 350, '\uf009', { fill : '#FFD700', font : '64px FontAwesome' });
    //this.randomRotatingIcon.anchor.setTo(0.5);

    /* adding Mute icon*/
    this.mute = this.add.text(0, 0, gameOptions.playMusic ? '\uf028':'\uf026', { fill :cs.accent_color, font : '40px FontAwesome'});
    this.adjustBottom(20, game.world.width, this.mute)

    makeIconBtn(this.mute, function (_mute) {
      console.log("Mute that ", music.volume)
        gameOptions.playMusic = !gameOptions.playMusic
        music.volume = gameOptions.playMusic ? 1 : 0;
        music.volume ? _mute.setText('\uf028') : _mute.setText('\uf026')
    },'randomCustom', fa_style)
  },

  update: function() {
    //this.randomRotatingIcon.angle += 1
    if (this.cursor.left.isDown) 
    {
      this.player.animations.play('damage'); 
    }
    else{this.player.animations.play('idle'); }
  }
  

};

Phaser.Utils.mixinPrototype(GameMenu.prototype, mixins);


function makeIconBtn (txt, callback, className, _style){
      txt.anchor.setTo(0.5);
      txt.inputEnabled = true;

      txt.events.onInputUp.add(callback);
      txt.events.onInputOver.add(function (target) {
        target.setStyle(fa_style.navitem.hover);
      });
      txt.events.onInputOut.add(function (target) {
        target.setStyle(fa_style.navitem[className]);
      });
}
