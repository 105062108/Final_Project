'use strict';

var Splash = function () {};
var dataStore = null;
    // playSound = true,
    // playMusic = true,
    // music;

Splash.prototype = {

  loadScripts: function () {
    game.load.script('style', 'lib/style.js');
    game.load.script('mixins', 'lib/mixins.js');
    // game.load.script('uuid', 'lib/bower_components/node-uuid/uuid.js');
    game.load.script('WebFont', 'vendor/webfontloader.js');
    game.load.script('menu','js/gamemenu.js');
    game.load.script('game', 'js/game.js');
    game.load.script('map', 'js/map_2.js');
    game.load.script('option', 'js/options.js');
    game.load.script('end','js/ending.js');
  },

  loadBgm: function () {
    // thanks Kevin Macleod at http://incompetech.com/
    game.load.audio('dangerous', 'assets/bgm/Analog Hero.mp3');
    game.load.audio('exit', 'assets/bgm/Exit the Premises.mp3');
  },
  // varios freebies found from google image search
  loadImages: function () {
    game.load.image('menu-bg', 'assets/images/menu-bg.jpg');
    game.load.image('options-bg', 'assets/images/options-bg.jpg');
    game.load.image('blur-bg', 'assets/images/blur32x32.png');
    game.load.image('background_in', 'assets/images/back.jpg');
    game.load.image('background_st', 'assets/images/setting_bg.jpg');
    game.load.spritesheet('warrior','assets/images/warrior.png',182,123);
    // game.load.image('gameover-bg', 'assets/images/gameover-bg.jpg');
  },

  loadFonts: function () {
    WebFontConfig = {
      custom: {
        families: ['TheMinion', 'FontAwesome'],
        urls: ['assets/style/theminion.css', 'assets/style/font-awesome.min.css']
      },
      google: {
          families: ['Sniglet', 'Modak']
      }
    }
  },
  /*connectDataStore: function(){
    dataStore = window.localStorage;
    if (dataStore){
      var _highScore = dataStore.getItem("_highScore") || 0;
      var _uuid = dataStore.getItem("_uuid") || uuid.v4();
      dataStore.setItem("_uuid", _uuid);
      console.log(_highScore, _uuid)
    }

  },*/
  init: function () {
    this.loadingBar = game.make.sprite(game.world.centerX-(387/2), 400, "loading");
    this.logo       = game.make.sprite(game.world.centerX, -110, 'brand');
    this.status     = game.make.text(game.world.centerX, 380, 'Loading...', {fill: 'white'});
    utils.centerGameObjects([this.logo, this.status]);
    game.add.existing(this.logo).scale.setTo(0.5);
  },

  preload: function () {
    // game.add.sprite(0, 0, 'stars');
    game.stage.backgroundColor = "#000000";//#31152b
    var tween = game.add.tween(this.logo);
    tween.to({y: game.world.centerY-50}, 1000).easing(Phaser.Easing.Bounce.Out).start();
    game.add.existing(this.loadingBar);
    game.add.existing(this.status);
    //this.connectDataStore()
    this.load.setPreloadSprite(this.loadingBar);

    this.loadScripts();
    this.loadImages();
    this.loadFonts();
    this.loadBgm();

  },


  
  addGameStates: function () {

    game.state.add("menu",GameMenu);
    game.state.add("map",MapState);
    game.state.add("option",Options);
    game.state.add("game",GameState);
    game.state.add("end",EndState);
  },

  addGameMusic: function () {
    music = game.add.audio('dangerous');
    music.loop = true;
    // music.play();
  },


  create: function() {
    this.status.setText('Ready!');
    this.addGameStates();
    this.addGameMusic();
    //this.initDeviceRatio();

    setTimeout(function () {
      game.state.start('menu');
    }, 3000);
  }
};
