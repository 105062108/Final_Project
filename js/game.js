var text_rpg = new Array(4);
var text_i = 0;
var GameState = {
    preload: function(){
        
    },
    create: function(){
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        //block
        this.background = game.add.sprite(0,0,'bg');

        this.block_array = ['block1', 'block2', 'block3', 'block4', 'block5', 'block6', 'block7'];
        this.check_blcok = [0,1,2,3,4,5,6];
        this.createBlock();
        this.falling = game.time.events.loop(1000, this.BlockFalling, this);
        //floor
        this.floor = game.add.group();
        this.floor.enableBody = true;
        for(var i=0;i<10;i++){
            var floor_block = game.add.sprite(i*18,18*29,'floor',0,this.floor);
            floor_block.body.immovable = true;
        }

        this.map = new Array(30);
        for(var t=0;t<29;t++){
            this.map[t] = [0,0,0,0,0,0,0,0,0,0];
        }
        this.map[29] = [1,1,1,1,1,1,1,1,1,1];
        
        this.obstacle = new Array(30);
        for (var t = 0; t < 30; t++) {
            this.obstacle[t] = new Array(10);
        }
        //the keyboard control
        this.control = {
            left: game.input.keyboard.addKey(Phaser.Keyboard.LEFT),
            right: game.input.keyboard.addKey(Phaser.Keyboard.RIGHT),
            down: game.input.keyboard.addKey(Phaser.Keyboard.DOWN),
            z: game.input.keyboard.addKey(Phaser.Keyboard.Z),
            x: game.input.keyboard.addKey(Phaser.Keyboard.X),
            space: game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR)
        };
        //use onDown to trigger (isDown will trigger function repeatedly)
        this.control.space.onDown.add(this.BlockControlDrop,this);
        this.control.left.onDown.add(this.BlockControlLeft,this);
        this.control.right.onDown.add(this.BlockControlRight,this);
        this.control.z.onDown.add(this.BlockContorlRotate,this);
        this.control.x.onDown.add(this.BlockContorlRotateCounterwise,this);
        this.control.down.onDown.add(this.BlockControlDown,this);
       
        this.stage_now = 1;

        this.rpg_bg = game.add.sprite(180,0,'rpg_bg');
        this.rpg_bg.scale.setTo(0.65,0.65);
        game.add.sprite(180,307,'row');
        game.add.sprite(0,0,'row');
        game.add.sprite(0,534,'row');
        game.add.sprite(180,397,'row');
        game.add.sprite(180, 0, 'line');
        game.add.sprite(180, 160, 'line');
        game.add.sprite(180, 320, 'line');
        game.add.sprite(180, 480, 'line');
        game.add.sprite(534, 0, 'line');
        game.add.sprite(534, 160, 'line');
        game.add.sprite(534, 320, 'line');
        game.add.sprite(534, 480, 'line');
        //player 
        this.player = game.add.sprite(380,120,'player');
        this.player.scale.setTo(-1,1);
        this.player.level = 1;
        this.player.health = 15;
        this.player.maxHealth = 15;
        this.player.onAttack = false;
        this.player.onDamage = false;
        this.player.onguard = false;

        this.player.animations.add('idle',[0,1,2,3,4,5],8,true);
        this.player.animations.add('attack',[6,7,8,9,10,11],8,false);
        this.player.animations.add('guard',[12,13,14,15,16,17,18,19,20],8,false);
        this.player.animations.add('damage',[21,22,23,24],8,false);
        this.player.animations.play('idle');
        //monster create
        this.snake = game.add.sprite(490,150,'snake');
        this.snake.scale.setTo(-0.8,0.8);
        this.snake.health = 10;
        this.snake.onidle = true;

        this.snake.animations.add('idle',[0,1,2,3,4,5],8,true);
        this.snake.animations.add('attack',[6,7,8,9,10,11],8,false);
        this.snake.animations.add('attack2',[12,13,14,15],8,false);
        this.snake.animations.add('damage',[16,17,18],8,false);
        this.snake.animations.play('idle');

        this.mushroom = game.add.sprite(700,120,'mushroom');
        this.mushroom.scale.setTo(-0.7,0.7);
        this.mushroom.health = 25;
        
        this.mushroom.animations.add('idle',[0,1,2,3,4,5,6],8,true);
        this.mushroom.animations.add('attack',[7,8,9,10,11,12,13,14,15,16,17,18,19],8,false);
        this.mushroom.animations.add('damage',[29,30,31,32],8,true);
        this.mushroom.animations.play('idle');
        //Player lifebar
        this.shell = game.add.sprite(266, 295, 'hpshell');
        this.shell.scale.setTo(0.3, 0.4);
        this.healthbar1 = game.add.sprite(268, 295, 'hp1');
        this.healthbar1.scale.setTo(0.3, 0.4);
        this.healthbar2 = game.add.sprite(268 + 18 * 7.63, 295, 'hp2');
        this.healthbar2.scale.setTo(0.3, 0.4);
        //monster lifebar 
        this.monster_shell = game.add.sprite(390,150,'life_bg');
        this.monster_hp = game.add.sprite(395,150,'life_bar');
        this.monster_life = this.snake.health;
        this.monster_Maxhealth = 10;

        this.char = game.add.sprite(186, 313, 'char');
        this.char.scale.setTo(0.7,0.7);
        /*this.basicAtk = game.add.sprite(335-10, 397, 'basic_atk');
        this.basicAtk.scale.setTo(0.9, 0.9);
        this.skill2 = game.add.sprite(415-10, 397, 'skill2');
        this.skill2.scale.setTo(0.9, 0.9);
        this.ult = game.add.sprite(415+20, 460, 'ultimate');
        this.ult.scale.setTo(0.9, 0.9);
        this.shield = game.add.sprite(335+20, 460, 'shield');
        this.shield.scale.setTo(0.9, 0.9);*/
        //monster Attack
        if(this.stage_now == 1){
            game.time.events.loop(5000,this.MonsterAutoAttack,this);
        }

        //bgm
        this.bgm = game.add.audio('bgm');
        this.bgm.loop = true;
        this.bgm.play();
        this.bgm.volume = 0.2;

    },
    update: function(){
        this.checkBounds();
        this.checkPosition();
        this.lifeControl();
        this.MonsterlifeControl();
    },
    returnIdle: function(){
        this.player.animations.play('idle');
        this.snake.animations.play('idle');
        this.mushroom.animations.play('idle');
    },
    checkBounds: function(){  
        if(this.new_block.onLand){
            var lose = false;
            this.new_block.forEach(function (child) {
                /*console.log((child.position.y + this.new_block.position.y) / 18);
                console.log((child.position.x + this.new_block.position.x) / 18);*/
                var y = (child.position.y + this.new_block.position.y) / 18;
                var x = (child.position.x + this.new_block.position.x) / 18;
                child.position.y = y * 18;
                if(y < 0){
                    this.LoseGame();
                    lose = true;
                    return;
                }
                this.map[y][x] = 1;
                this.obstacle[y][x] = child;
            }, this);
            this.new_block.position.y = 0;
            if(this.check_block.length!=0){
                this.check_block.kill();
            }
            //console.log(this.obstacle);
            if(!lose){
                this.checkClear();
                game.time.events.pause(this.falling);
                this.createBlock();
                game.time.events.resume(this.falling);
            }
        }
    },
    createBlock: function(){
        //block rule
        this.check_block = game.add.group();
        this.number = game.rnd.pick(this.check_blcok);
        var index = this.check_blcok.indexOf(this.number);
        this.check_blcok.splice(index,1);
        if(this.check_blcok.length == 0)
            this.check_blcok = [0, 1, 2, 3, 4, 5, 6];
        //console.log(this.check_blcok);
        this.new_block = game.add.group();
        this.new_block.enableBody = true;
        //setting the block's shape
        this.createShape(this.number);
        //other setting 
        this.new_block.position.x = 90;
        this.new_block.position.y = 0;
        this.new_block.onLand = false;
    },
    createShape: function(type){
        switch(type){
            case 0:
                game.add.sprite(0, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(-18, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(-18, -18, this.block_array[type], 0, this.new_block);
                game.add.sprite(18, 0, this.block_array[type], 0, this.new_block);
                break;
            case 1:
                game.add.sprite(0, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(0, -18, this.block_array[type], 0, this.new_block);
                game.add.sprite(-18, -18, this.block_array[type], 0, this.new_block);
                game.add.sprite(18, 0, this.block_array[type], 0, this.new_block);
                break;
            case 2:
                game.add.sprite(0, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(0, -18, this.block_array[type], 0, this.new_block);
                game.add.sprite(0, -36, this.block_array[type], 0, this.new_block);
                game.add.sprite(0, 18, this.block_array[type], 0, this.new_block);
                break;
            case 3:
                game.add.sprite(0, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(-18, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(18, -18, this.block_array[type], 0, this.new_block);
                game.add.sprite(18, 0, this.block_array[type], 0, this.new_block);
                break;
            case 4:
                game.add.sprite(0, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(-18, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(0, -18, this.block_array[type], 0, this.new_block);
                game.add.sprite(18, 0, this.block_array[type], 0, this.new_block);
                break;
            case 5:
                game.add.sprite(0, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(-18, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(0, -18, this.block_array[type], 0, this.new_block);
                game.add.sprite(18, -18, this.block_array[type], 0, this.new_block);
                break;
            case 6:
                game.add.sprite(0, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(-18, 0, this.block_array[type], 0, this.new_block);
                game.add.sprite(-18, -18, this.block_array[type], 0, this.new_block);
                game.add.sprite(0, -18, this.block_array[type], 0, this.new_block);
                break;
        }
    },
    BlockControlLeft: function(){
        var canLeft = true;
        this.new_block.forEach(function(child){
            var x = (child.position.x + this.new_block.position.x ) / 18;
            var y = (child.position.y + this.new_block.position.y) / 18;
            if (y < 0) y = 0;
            if(x == 0){
                canLeft = false;
            }
            if(this.map[y][x-1]==1){
                canLeft = false;
            }
        },this);

        if(canLeft) this.new_block.position.x -= 18;
    },
    BlockControlRight: function () {
        var canRight = true;
        this.new_block.forEach(function (child) {
            var x = (child.position.x + this.new_block.position.x) / 18;
            var y = (child.position.y + this.new_block.position.y) / 18;
            if(y<0) y = 0;
            if (x == 9) {
                canRight = false;
            }
            if(this.map[y][x+1]==1){
                canRight = false;
            }
        },this);

        if(canRight) this.new_block.position.x += 18;
    },
    BlockContorlRotate: function(){
        var canrotate = true;
        this.new_block.forEach(function (child) {
            var x = (-child.position.y + this.new_block.position.x) / 18;
            if(x<0 || x>9){
                canrotate = false;
            }
        }, this);
        if(canrotate){
            this.new_block.forEach(function (child) {
                var swap = child.position.x;
                child.position.x = -child.position.y;
                child.position.y = swap;
            }, this);
            this.check_block.forEach(function (child) {
                var swap = child.position.x;
                child.position.x = -child.position.y;
                child.position.y = swap;
            }, this);
        }
    },
    BlockContorlRotateCounterwise: function(){
        var canrotate = true;
        this.new_block.forEach(function (child) {
            var x = (child.position.y + this.new_block.position.x) / 18;
            if (x < 0 || x > 9) {
                canrotate = false;
            }
        }, this);
        if (canrotate) {
            this.new_block.forEach(function (child) {
                var swap = child.position.x;
                child.position.x = child.position.y;
                child.position.y = -swap;
            }, this);
            this.check_block.forEach(function (child) {
                var swap = child.position.x;
                child.position.x = child.position.y;
                child.position.y = -swap;
            }, this);
        }
    },
    BlockControlDown: function(){
        
        var canMove = true;
        this.new_block.forEach(function (child) {
            //console.log(this.map[(child.position.y + this.new_block.position.y + 18) / 18][(child.position.x + this.new_block.position.x + 18) / 18]);
            var y = (child.position.y + this.new_block.position.y + 18) / 18;
            if (y < 0) y = 0;
            var x = (child.position.x + this.new_block.position.x) / 18;
            if (this.map[y][x] == 1) {
                canMove = false;
                return;
            }
        }, this);
        if(canMove) this.new_block.position.y += 18;
    },
    BlockControlDrop: function(){
        while(this.new_block.onLand == false){
            this.BlockFalling();
        }
        return this.new_block.position.y;
    },
    BlockFalling: function(){
        var canFall = true;
        //console.log(this.map);
        this.new_block.forEach(function(child){
            //console.log(this.map[(child.position.y + this.new_block.position.y + 18) / 18][(child.position.x + this.new_block.position.x + 18) / 18]);
            var y = (child.position.y + this.new_block.position.y + 18) / 18;
            if(y<0) y=0;
            //console.log(y);
            var x = (child.position.x + this.new_block.position.x ) / 18;
            if (this.map[y][x] == 1 ){
                this.new_block.onLand = true;
                canFall = false;
                return;
            }
        },this);
        if (canFall) {
            this.new_block.position.y += 18;
        }
    },
    checkClear: function(){
        var hit = 0;
        //console.log(this.obstacle);
        //console.log(this.map);
        //console.log(this.obastacle[28][5].world);
        for(var i=0;i<29;i++){
            if(this.map[i].indexOf(0) == -1){
                hit += 1;
                //console.log('find');
                for(var j=0;j<=9;j++){
                    this.obstacle[i][j].kill();
                    this.obstacle[i][j] = null;
                }
                this.map[i] = [0,0,0,0,0,0,0,0,0,0];
                for (var t = i; t > 0; t--) {
                    this.map[t] = this.map[t - 1];
                    for(var m=0;m<10;m++){
                        if(this.obstacle[t-1][m]){
                            this.obstacle[t-1][m].position.y += 18;
                        }
                    }
                    this.obstacle[t] = this.obstacle[t-1];
                }
            }
        }  
        if(hit!=0){
            this.playerAttack(hit);
        }
    },
    playerAttack: function(hit){
        //game.add.tween(this.player).to({x:190},200).yoyo(true).start();
        var attack = this.player.animations.play('attack');
        if(hit!=3) {
            if(this.stage_now == 1){
                var damage = this.snake.animations.play('damage');
                game.add.tween(this.snake).to({
                    tint: 0xff0000
                }, 300).easing(Phaser.Easing.Exponential.InOut).yoyo(true).start();
            }
            else{
                var damage = this.mushroom.animations.play('damage');
                game.add.tween(this.mushroom).to({
                    tint: 0xff0000
                }, 300).easing(Phaser.Easing.Exponential.InOut).yoyo(true).start();
            }
        }
        attack.onComplete.add(this.returnIdle,this);
        
        if (hit == 1) {
            one = game.add.audio('one');
            one.play();
            one.volume = 0.2;
            this.basic_attack = game.add.sprite(510, 160, 'atk1');
            this.basic_attack.scale.x = -1;
            this.basic_attack.frame = 0;
            this.basic_attack.animations.add('atk', [0, 1, 2, 3, 4, 5, 6], 10);
            this.basic_attack.animations.play('atk');
            game.time.events.add(Phaser.Timer.SECOND * 0.5, function () {
                this.basic_attack.kill();
            }, this);
            var style = {fill: 'snow', fontSize: '16px'}
            var fir = 'Warrior deals 1 damage to the enemy!';
            if(text_i == 0) {
                game.add.sprite(200, 450, 'black');
            }
            game.add.text(200, 450 + 18 * text_i, fir, style);
            text_rpg[text_i] = fir;
            if(text_i == 3) {
                text_i = 0;
            }
            else {
                text_i = text_i + 1;
            }
            if (this.stage_now == 1) {
                this.snake.damage(2);
                this.monster_life -= 2;
                this.dmg1 = game.add.sprite(450, 160, 'num');
                this.dmg1.scale.setTo(2, 2);
                this.dmg1.frame = 2;
                game.physics.arcade.enable(this.dmg1);
                this.dmg1.body.velocity.x = 40;
                this.dmg1.body.velocity.y = -40;
                this.dmg1.body.gravity.y = 1000;
                game.time.events.add(Phaser.Timer.SECOND * 0.2, function () {
                    this.dmg1.body.velocity.y = -130;
                    this.dmg1.body.gravity.y = 500;
                }, this);
                game.time.events.add(Phaser.Timer.SECOND * 0.6, function () {
                    game.add.tween(this.dmg1).to({ alpha: 0 }, 400, Phaser.Easing.Linear.None, true);
                }, this);
            } else {
                this.mushroom.damage(3);
                this.monster_life -= 3;
                this.dmg1 = game.add.sprite(450, 160, 'num');
                this.dmg1.scale.setTo(2, 2);
                this.dmg1.frame = 3;
                game.physics.arcade.enable(this.dmg1);
                this.dmg1.body.velocity.x = 40;
                this.dmg1.body.velocity.y = -40;
                this.dmg1.body.gravity.y = 1000;
                game.time.events.add(Phaser.Timer.SECOND * 0.2, function () {
                    this.dmg1.body.velocity.y = -130;
                    this.dmg1.body.gravity.y = 500;
                }, this);
                game.time.events.add(Phaser.Timer.SECOND * 0.6, function () {
                    game.add.tween(this.dmg1).to({ alpha: 0 }, 400, Phaser.Easing.Linear.None, true);
                }, this);
            }
        }
        if (hit == 2) {
            two = game.add.audio('two');
            two.play();
            two.volume = 0.2;
            this.attack_two = game.add.sprite(400, 160, 'atk2');
            this.attack_two.frame = 0;
            this.attack_two.animations.add('atk', [0, 1, 2, 3, 4, 5], 10);
            this.attack_two.animations.play('atk');
            game.time.events.add(Phaser.Timer.SECOND * 0.5, function () {
                this.attack_two.kill();
            }, this);
            if (this.stage_now == 1) {
                this.snake.damage(4);
                this.monster_life -= 4;
                this.dmg1 = game.add.sprite(450, 160, 'num');
                this.dmg1.scale.setTo(2, 2);
                this.dmg1.frame = 4;
                game.physics.arcade.enable(this.dmg1);
                this.dmg1.body.velocity.x = 40;
                this.dmg1.body.velocity.y = -40;
                this.dmg1.body.gravity.y = 1000;
                game.time.events.add(Phaser.Timer.SECOND * 0.2, function () {
                    this.dmg1.body.velocity.y = -130;
                    this.dmg1.body.gravity.y = 500;
                }, this);
                game.time.events.add(Phaser.Timer.SECOND * 0.6, function () {
                    game.add.tween(this.dmg1).to({ alpha: 0 }, 400, Phaser.Easing.Linear.None, true);
                }, this);
            } else {
                this.mushroom.damage(5);
                this.monster_life -= 5;
                this.dmg1 = game.add.sprite(450, 160, 'num');
                this.dmg1.scale.setTo(2, 2);
                this.dmg1.frame = 5;
                game.physics.arcade.enable(this.dmg1);
                this.dmg1.body.velocity.x = 40;
                this.dmg1.body.velocity.y = -40;
                this.dmg1.body.gravity.y = 1000;
                game.time.events.add(Phaser.Timer.SECOND * 0.2, function () {
                    this.dmg1.body.velocity.y = -130;
                    this.dmg1.body.gravity.y = 500;
                }, this);
                game.time.events.add(Phaser.Timer.SECOND * 0.6, function () {
                    game.add.tween(this.dmg1).to({ alpha: 0 }, 400, Phaser.Easing.Linear.None, true);
                }, this);
            }
            var style = {fill: 'snow', fontSize: '16px'}
            var fir = 'Warrior raises his sword, Yuwei!';
            if(text_i == 0) {
                game.add.sprite(200, 450, 'black');
            }
            game.add.text(200, 450 + 18 * text_i, fir, style);
            text_rpg[text_i] = fir;
            if(text_i == 3) {
                text_i = 0;
            }
            else {
                text_i = text_i + 1;
            }
        }
        if (hit == 3) {
            this.player.animations.play('guard');
            this.player.onguard = true;
            var style = {fill: 'snow', fontSize: '16px'}
            var fir = 'Warrior GUARD!';
            if(text_i == 0) {
                game.add.sprite(200, 450, 'black');
            }
            game.add.text(200, 450 + 18 * text_i, fir, style);
            text_rpg[text_i] = fir;
            if(text_i == 3) {
                text_i = 0;
            }
            else {
                text_i = text_i + 1;
            }
        }
        if (hit == 4) {
            this.attack_ef = game.add.sprite(400, 160, 'atk4');
            this.attack_ef.frame = 0;
            this.attack_ef.animations.add('atk', [0, 1, 2, 3, 4], 8);
            this.attack_ef.animations.play('atk');
            game.time.events.add(Phaser.Timer.SECOND * 0.5, function () {
                this.attack_ef.kill();
            }, this);
            var style = {fill: 'salmon', fontSize: '16px', font: 'serif'}
            var fir = 'Blow up!! All of our FINALS QAQ!!';
            if(text_i == 0) {
                game.add.sprite(200, 450, 'black');
            }
            game.add.text(200, 450 + 18 * text_i, fir, style);
            text_rpg[text_i] = fir;
            if(text_i == 3) {
                text_i = 0;
            }
            else {
                text_i = text_i + 1;
            }
            game.camera.shake();
            if (this.stage_now == 1){
                this.snake.damage(8);
                this.monster_life -= 8;
                this.dmg1 = game.add.sprite(450, 160, 'num');
                this.dmg1.scale.setTo(2, 2);
                this.dmg1.frame = 8;
                game.physics.arcade.enable(this.dmg1);
                this.dmg1.body.velocity.x = 40;
                this.dmg1.body.velocity.y = -40;
                this.dmg1.body.gravity.y = 1000;
                game.time.events.add(Phaser.Timer.SECOND * 0.2, function () {
                    this.dmg1.body.velocity.y = -130;
                    this.dmg1.body.gravity.y = 500;
                }, this);
                game.time.events.add(Phaser.Timer.SECOND * 0.6, function () {
                    game.add.tween(this.dmg1).to({ alpha: 0 }, 400, Phaser.Easing.Linear.None, true);
                }, this);
            }
            else {
                this.mushroom.damage(9);
                this.monster_life -= 9;
                this.dmg1 = game.add.sprite(450, 160, 'num');
                this.dmg1.scale.setTo(2, 2);
                this.dmg1.frame = 9;
                game.physics.arcade.enable(this.dmg1);
                this.dmg1.body.velocity.x = 40;
                this.dmg1.body.velocity.y = -40;
                this.dmg1.body.gravity.y = 1000;
                game.time.events.add(Phaser.Timer.SECOND * 0.2, function () {
                    this.dmg1.body.velocity.y = -130;
                    this.dmg1.body.gravity.y = 500;
                }, this);
                game.time.events.add(Phaser.Timer.SECOND * 0.6, function () {
                    game.add.tween(this.dmg1).to({ alpha: 0 }, 400, Phaser.Easing.Linear.None, true);
                }, this);
            }
        }
        if(this.stage_now == 1){
            if(!this.snake.alive){
                this.stage_now++;
                game.time.events.add(1000,this.MonsterGetIn,this);
            }
        }
    },
    MonsterGetIn: function(){
        if(this.stage_now == 2){
            game.add.tween(this.mushroom).to({
                x: 510
            }, 300).easing(Phaser.Easing.Exponential.InOut).start();
            this.monster_life = this.mushroom.health;
            this.monster_Maxhealth = 25;
            this.player.health = 35;
            this.player.maxHealth  = 35;
            this.player.level = 2;
        }
    },
    MonsterAutoAttack:function(){
        if(this.stage_now == 1 && this.snake.alive){
            var attack = this.snake.animations.play('attack');
            
            if (!this.player.onguard) {
                var damage = this.player.animations.play('damage');
                this.player.damage(2);
            }
            this.player.onguard = false;
            attack.onComplete.add(this.returnIdle, this);
            game.add.tween(this.snake).to({x:this.snake.position.x-10},200).yoyo(true).start();
            game.add.tween(this.player).to({
                tint: 0xff0000
            }, 150).easing(Phaser.Easing.Exponential.Out).yoyo(true).start();

        }
        if (this.stage_now == 2 && this.mushroom.alive) {
            var attack = this.mushroom.animations.play('attack');
            if (!this.player.onguard) {
                var damage = this.player.animations.play('damage');
                this.player.damage(7);
            }
            this.player.onguard = false;
            attack.onComplete.add(this.returnIdle, this);
            game.add.tween(this.snake).to({
                x: this.snake.position.x - 10
            }, 200).yoyo(true).start();
            game.add.tween(this.player).to({
                tint: 0xff0000
            }, 150).easing(Phaser.Easing.Exponential.Out).yoyo(true).start();

        }
        if(!this.player.alive){
            this.LoseGame();
        }
    },
    checkPosition: function(){
        if(!this.check_block.length ){
            switch(this.number){
                case 0:
                    game.add.sprite(0, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(-18, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(-18, -18, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(18, 0, this.block_array[this.number], 0, this.check_block);
                    break;
                case 1:
                    game.add.sprite(0, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(0, -18, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(-18, -18, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(18, 0, this.block_array[this.number], 0, this.check_block);
                    break;
                case 2:
                    game.add.sprite(0, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(0, -18, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(0, -36, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(0, 18, this.block_array[this.number], 0, this.check_block);
                    break;
                case 3:
                    game.add.sprite(0, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(-18, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(18, -18, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(18, 0, this.block_array[this.number], 0, this.check_block);
                    break;
                case 4:
                    game.add.sprite(0, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(-18, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(0, -18, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(18, 0, this.block_array[this.number], 0, this.check_block);
                    break;
                case 5:
                    game.add.sprite(0, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(-18, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(0, -18, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(18, -18, this.block_array[this.number], 0, this.check_block);
                    break;
                case 6:
                    game.add.sprite(0, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(-18, 0, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(-18, -18, this.block_array[this.number], 0, this.check_block);
                    game.add.sprite(0, -18, this.block_array[this.number], 0, this.check_block);
                    break;
            }
        }
        
        this.check_block.alpha=0.3;
        this.check_block.position.x = this.new_block.position.x;
        
        this.y_final = this.new_block.position.y;
        this.checkMove = true;
        while (this.checkMove) {
            this.new_block.forEach(function (child) {
                //console.log(this.map[(child.position.y + this.new_block.position.y + 18) / 18][(child.position.x + this.new_block.position.x + 18) / 18]);
                var y = (child.position.y + this.y_final + 18) / 18;
                if (y < 0) y = 0;
                var x = (child.position.x + this.new_block.position.x) / 18;
                if (this.map[y][x] == 1) {
                    this.checkMove = false;
                    return;
                }
            }, this);
            if (this.checkMove) this.y_final += 18;
        }
        this.check_block.position.y = this.y_final;
       //  console.log(this.check_block.length);
    },
    lifeControl: function(){
        console.log(this.player.maxHealth);
        if(this.player.health<0){
            this.healthbar1.width = 0;
            this.healthbar2.width = 0;
        }
        else if (this.player.health > 7.5) {
            this.healthbar1.width = 1 * 18 * 7.9;
            this.healthbar2.width = ((this.player.health - this.player.maxHealth / 2) / (this.player.maxHealth / 2)) * 18 * 6.93;
        } else {
            this.healthbar1.width = ((this.player.health) / 7.5) * 18 * 7.9;
            this.healthbar2.width = 0 ;
        }
    },
    MonsterlifeControl: function(){
        if(this.monster_life<=0){
            this.monster_hp.width = 0;
        }
        else{
            this.monster_hp.width = 100 * (this.monster_life)/this.monster_Maxhealth;
        }
    },
    LoseGame: function(){
        this.bgm.stop();
        game.state.start("end");
    }
};
